# This includes the bashrc distributed by 98-172
# Great Practical Ideas for Computer Scientists
source ~/.bashrc_gpi

# Add your own changes below...
PATH="/afs/cs.cmu.edu/academic/class/15410-f18/private/bin:/afs/cs.cmu.edu/academic/class/15410-f18/bin:/afs/cs.cmu.edu/academic/class/15410-f18/private/scripts:${PATH}";
export PATH



# Connect for 410
aklog cs.cmu.edu

export SIMICS_REALTIME="yes"
# export SIMICS_TEXT_CONSOLE="yes"

alias s="simics46 --sync"

# Facebook Stuff
shopt -s checkwinsize

if [ -f ~/settings/scm-prompt.sh ]; then
  source ~/settings/scm-prompt.sh
fi

#export PS1='\u@\h:\W $(_scm_prompt)\$ '

function parse_hg_branch {
  if [[ -n $(_scm_prompt) ]]; then
    # wrap in parens
    echo "$(_scm_prompt)"
  fi
}

# Show current hg bookmark
function hgproml {
  # here are a bunch of colors in case
  # you want to get colorful
  local      YELLOW="\[\033[0;33m\]"
  local        BLUE="\[\033[0;34m\]"
  local         RED="\[\033[0;31m\]"
  local   LIGHT_RED="\[\033[1;31m\]"
  local       GREEN="\[\033[0;32m\]"
  local LIGHT_GREEN="\[\033[1;32m\]"
  local      PURPLE="\[\033[0;35m\]"
  local        CYAN="\[\033[0;36m\]"
  local       WHITE="\[\033[1;37m\]"
  local  LIGHT_GRAY="\[\033[0;37m\]"
  local RESET_COLOR="\[\033[0m\]"

  export PS1="[Exit: \[\033[1;31m\]\${PIPESTATUS[@]/#0/\[\033[0m\]\[\033[1;32m\]0\[\033[1;31m\]}\[\033[0m\]]
$GREEN[$RED\t $PURPLE\u@\h $YELLOW\w$CYAN\$(parse_hg_branch)$GREEN]\
\$$RESET_COLOR "
PS2='> '
PS4='+ '
}
hgproml

HISTSIZE=130000 HISTFILESIZE=-1

alias rmux="tmux new-session -A -D -s 'rmux'"

alias cp='cp --backup=numbered'
alias ln='ln --backup=numbered'
alias mv='mv -f --backup=numbered'

# Only modify stty if the current shell is interactive
[[ $- == *i* ]] && stty stop undef
[[ $- == *i* ]] && stty start undef

# Stuff from James
export CLICOLOR=true
#export PS1="\[$(tput setaf 2)\][\[$(tput setaf 1)\]\t \u\[$(tput setaf 2)\]@\[$(tput setaf 1)\]$SHORTHOSTNAME \[$(tput setaf 6)\]\w\[$(tput setaf 2)\]]\n\$\[$(tput sgr0)\] "
export LSCOLORS="exfxcxdxbxegedabagacad"
export LS_COLORS="no=00:\
fi=00:\
di=01;36:\
ln=01;36:\
pi=40;33:\
so=01;35:\
do=01;35:\
bd=40;33;01:\
cd=40;33;01:\
or=40;31;01:\
ex=01;32:\
*.tar=01;33:*.tgz=01;33:*.arj=01;33:*.taz=01;33:*.lzh=01;33:*.zip=01;33:*.gz=01;33:*.bz2=01;33:*.deb=01;33:*.rpm=01;33:*.jar=01;33:\
*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:\
*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:\
*.ogg=01;35:*.mp3=01;35:*.wav=01;35:\
";

# Avoid duplicates
export HISTCONTROL=ignoredups:erasedups
# When the shell exits, append to the history file instead of overwriting it
shopt -s histappend

# After each command, append to the history file and reread it
#export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

hall() { history | grep $1; }

# default arguments
alias l='ls -AG'
# Long listing
alias ll='ls -aAlGh'
# Newest at bottom
alias lt='ls -AltrGhu'
# Largest at bottom
alias lz='ls -AlGrhS'
# Extended attributes
alias lx='ls -aAlGh@O'
# Do a grep afterwards
alias lg='ls -AlGOh@ | grep -i '
# everything
alias le='ls -aAlGh@eOR'

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias dl="cd ~/Downloads"
alias dt="cd ~/Desktop"

# Concatenate and print content of files (add line numbers)
alias catn="cat -n"

# Grep
alias g='grep -H -i -n --color=always'
# Don't stick anything in grep options to ensure scripts can still use grep normally
export GREP_OPTIONS=
function hl () {
  grep --color=always -E "$1|\$"
}

# Base tools
alias hex='printf "%X\n" '
dec () {
  input=$(echo $1 | tr 'a-z' "A-Z")
  echo "ibase=16; $input" | bc
}

# ECE Cluster
alias m='make'

# Configure Bash Settings
# shopt -s autocd
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s histappend
shopt -s histreedit
shopt -s histverify

alias path='echo -e ${PATH//:/\\n}'
alias reload='source ~/.bash_profile'

# git
alias gs="git status "
alias gls="git log "
alias gbls="git branch -v"

alias gpl="git pull "
alias gps="git push"
alias gco="git checkout "

alias gdc="git checkout -- "
alias grh="git reset HEAD "

alias gaa="git add --all "
alias ga="git add "
alias gcf='git-clang-format --binary $(xcrun -f clang-format) --style=file'
alias gaf='gaa && gcf'
alias gci="git commit"
alias glh='git log --oneline --invert-grep --grep fixup! -n 1 --pretty=format:"%H"'
alias gf='git commit --fixup $(glh)'

alias grd='git rev-parse --show-toplevel | rev | cut -d"/" -f1 | rev | sed -e "s/-.*$//"'

alias gg="git grep -n "
alias gd="git diff "
alias gdh='git diff HEAD'
alias gdh1='git diff HEAD~1'
